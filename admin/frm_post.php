<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Post</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <nav id="formulario-menor">
        <form action="op_post.php" name="frmpost" id="frmpost" method="POST">
            <fieldset>
                <label for="">
                    <span>Categoria</span>
                    <?php 
                        require_once('../config.php');
                        $cats = Categoria::getList();
                    ?>
                    <select name="id_categoria" id="categoria">
                        <?php 
                            foreach($cats as $cat){
                        ?>
                        <option value="<?php echo $cat['id_categoria'];?>">
                            <?php echo $cat['categoria'];?>
                        </option>
                        <?php } ?>
                    </select>
                </label>
                <label for="">
                    <span>Titulo do post</span>
                    <input type="text" name="titulo_post" id="titulo_post">
                </label>
                <label for="">
                    <span>Descrição</span>
                    <input type="text" name="descricao_post" id="descricao_post">
                </label>
                <label for="">
                    <span>Imagem</span>
                    <input type="file" name="img_post" id="img_post">
                </label>
                <label for="">
               <label for="">
                    <span>Visitas</span>
                    <input type="number" name="visita_post" id="visita_post">
                </label>
                <label for="">
                    <span>Data da Post</span>
                    <input type="date" name="data_post" id="data_post">
                </label>
                    <span>Ativo</span>
                    <input type="checkbox" name="ativo_post" id="ativo_post">
                </label>
                <input type="submit" name="btn_cadastrar_post" value="Cadastrar post" class="botao">
                <span><?php echo isset($_GET['msg'])?'Sucesso':''; ?></span>
            </fieldset>
        </form>
    </nav>
</body>
</html>