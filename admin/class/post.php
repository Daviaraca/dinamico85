<?php
    /*  
        Autor: Edson M.R Luz
        Classe: TI-85
        data: 05/10/2019
        objetivo: 
            Criando classes em php, integrado com banco de dados
            Utilizar está e outras classes para futuras duvidas, 
            e aprendizado.  
    */
    class Post
    {
        #Atributos
        private $id_post;
        private $idCategoria;
        private $titulo;
        private $descricao;
        private $img;
        private $visitas;
        private $data;
        private $ativo;
        #Métodos de acesso

        #Métodos de acesso do atributo ID
        public function getId()
        {
            return $this->id_post;
        }
        public function setId($value)
        {
            $this->id_post = $value;
        }
        #Métodos de acesso do atributo ID Categoria
        public function getIdCategoria()
        {
            return $this->idCategoria;
        }
        public function setIdCategoria($value)
        {
            $this->idCategoria = $value;
        }
        #Métodos de acesso do atributo Titulo
        public function getTitulo()
        {
            return $this->titulo;
        }
        public function setTitulo($value)
        {
            $this->titulo = $value;
        }
        #Métodos de acesso do atributo Descricao
        public function getDescricao()
        {
            return $this->descricao;
        }
        public function setDescricao($value)
        {
            $this->descricao = $value;
        }
        #Métodos de acesso do atributo Img
        public function getImg()
        {
            return $this->img;
        }
        public function setImg($value)
        {
            $this->img = $value;
        }
        #Métodos de acesso do atributo Visitas
        public function getVisitas()
        {
            return $this->visitas;
        }
        public function setVisitas($value)
        {
            $this->visitas = $value;
        }
        #Métodos de acesso do atributo Data
        public function getData()
        {
            return $this->data;
        }
        public function setData($value)
        {
            $this->data = $value;
        }
        #Métodos de acesso do atributo Ativo
        public function getAtivo()
        {
            return $this->ativo;
        }
        public function setAtivo($value)
        {
            $this->ativo = $value;
        }
        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM post WHERE id = :id",array(':id'=>$_id));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        

        #Métodos para Inserir post
        public function inserirPost()
        {
            $sql = new Sql();
            $results = $sql->select("CALL sp_post_insert(:idcategoria, :titulo, :descricao, :img, :data_post, :ativo)",
            array(
                ":idcategoria"=>$this->getIdCategoria(),
                ":titulo"=>$this->getTitulo(),
                ":descricao"=>$this->getDescricao(),
                ":img"=>$this->getImg(),
                ":data_post"=>$this->getData(),
                ":ativo"=>$this->getAtivo()
            ));
            if(count($results)>0)
            {
                $this->setDados($results[0]);
            }
            
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select("SELECT * FROM post ORDER BY id_post");
        }
        #Atualizando post
        public function updatePost($_id_post,$_idCategoria,$_titulo,$_descricao,$_img,$_visitas,$_data,$_ativo)
        {
            $sql = new Sql();
            $sql->query('UPDATE post set id_categoria = :id_categoria ,titulo_post = :titulo,descricao_post = :descricao,img_post = :img
            ,visitas = :visitas,data_post = :data_post,post_ativo = :ativo, visitas = :visitas where id_post = :id',
            array(
                ':id_categoria'=>$_idCategoria,
                ':titulo'=>$_titulo,
                ':descricao'=>$_descricao,
                ':img'=>$_img,
                ':data_post'=>$_data,
                ':ativo'=>$_ativo,
                ':id_post'=>$_id_post,
                ':visitas'=>$_visitas));
        }
        #Deletando post
        public function deletePost($_id_post)
        {
            $sql = new Sql();
            $sql->query('DELETE FROM post where id_post = :id',array(':id'=>$_id_post));
        }
        #Consultando post por id
        public function consultarIdPost($_id_post)
        {
            $sql = new Sql();
            return $sql->select('select * from post where id_post = :id',array(':id'=>$_id_post));
        }
        #Consultando post por titulo
        public function consultarTituloPost($_titulo)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post where titulo_post LIKE :titulo',array(':titulo'=>'%'.$_titulo.'%'));
        }
        #consultando post por descricao
        public function consultarDescricaoPost($_descricao)
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post where descricao_post LIKE :descricao',array(':descricao'=>'%'.$_descricao.'%'));
        }
        #Listando post
        public function listarPost()
        {
            $sql = new Sql();
            return $sql->select('SELECT * FROM post');
        }
        #Listando poster com inner Join na tabela Categoria
        public function listarPostInner()
        {
            $sql = new Sql();
            return $sql->select('select a.id_post, b.categoria, a.titulo_post, a.descricao_post,a.img_post,a.visitas,
            a.data_post, a.post_ativo from post a INNER JOIN categoria b on a.id_categoria = b.id_categoria');
        }
        #Método construtor da class post
        public function __construct($_id_post='',$_idCategoria='',$_titulo='',$_descricao='',$_img='',$_visitas='',$_data='',$_ativo='')
        {
            $this->id_post = $_id_post;
            $this->idCategoria = $_idCategoria;
            $this->titulo = $_titulo;
            $this->descricao = $_descricao;
            $this->img = $_img;
            $this->visitas = $_visitas;
            $this->data = $_data;
            $this->ativo = $_ativo;
        }

        public function setDados($dados)
        {
            $this->id_post = $dados['id_post'];
            $this->idCategoria = $dados['id_categoria'];
            $this->titulo = $dados['titulo_post'];
            $this->descricao = $dados['descricao_post'];
            $this->img = $dados['img_post'];
            $this->visitas = $dados['visitas'];
            $this->data = $dados['data_post'];
            $this->ativo = $dados['post_ativo'];
        }
    }
?>