<?php
class Noticia{
   
    public $id_noticia;
    public $id_Categoria;
    public $tituloNoticia;
    public $imgNoticia;
    public $visitaNoticia;
    public $dataNoticia;
    public $noticiaAtivo;
    public $noticia;

    public function getIdNot()
        {
            return $this->id_noticia;
        }
        public function setIdNot($value)
        {
            $this->id_noticia = $value;
        
        }

        public function getIdcat()
        {
            return $this->id_categoria;
        }
        public function setIdCat($value)
        {
            $this->id_categoria = $value;
        
        }

        public function getTitulo()
        {
            return $this->titulooticia;
        }
        public function setTitulo($value)
        {
            $this->titulonoticia = $value;
        
        }

        public function getImg()
        {
            return $this->imgnoticia;
        }
        public function setImg($value)
        {
            $this->imgnoticia = $value;
        
        }

        public function getVisita()
        {
            return $this->visitanoticia;
        }
        public function setVisita($value)
        {
            $this->visitanoticia = $value;
        
        }

        public function getData()
        {
            return $this->datanoticia;
        }
        public function setData($value)
        {
            $this->datanoticia = $value;
        
        }

        public function getNotAtiv()
        {
            return $this->noticiaativo;
        }
        public function setNotAtiv($value)
        {
            $this->noticiaativo = $value;
        
        }

        public function getNoticia()
        {
            return $this->noticia;
        }
        public function setNoticia($value)
        {
            $this->noticia = $value;
        
        }
      

    public static function loadById($id_noticia){
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM noticias WHERE id_noticia = :id",
        array(":id"=>$id_noticia));
        if (count($results)>0){
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM noticias ORDER BY id_noticia");
    }

    public static function search($tituloNoticia){
        $sql = new Sql();
        return $sql->select("SELECT * FROM noticias WHERE titulo_noticia LIKE :titulo",
                array(":titulo"=>"%".$tituloNoticia."%"));
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_noticia_insert(:id_Categoria, :titulo_Noticia, :img_Noticia, :visita_Noticia, :data_Noticia, :noticia_Ativo, :noticia)",
        array(
            ":id_Categoria"=>$this->id_Categoria,
            ":titulo_Noticia"=>$this->tituloNoticia,
            ":img_Noticia"=>$this->imgNoticia,
            ":visita_Noticia"=>$this->visitaNoticia,
            ":data_Noticia"=>$this->dataNoticia,
            ":noticia_Ativo"=>$this->noticiaAtivo,
            ":noticia"=>$this->noticia
        ));
        if (count($results)>0){
            return $results[0];
        }
    }

    public function update($id, $idCategoria, $tituloNoticia, $imgNoticia, $dataNoticia, $noticiaAtivo, $noticia){
        $sql = new Sql();
        $sql->query("UPDATE noticias SET 
        id_categoria = :idcategoria, 
        titulo_noticia = :tituloNoticia, 
        img_noticia = :imgNoticia, 
        data_noticia = :dataNoticia, 
        noticia_ativo = :noticiaAtivo, 
        noticia = :noticia WHERE id_noticia = :id",
        array(
            ":id_noticia"=>$id,
            ":id_categoria"=>$idCategoria,
            ":titulo_Noticia"=>$tituloNoticia,
            ":img_Noticia"=>$imgNoticia,
            ":data_Noticia"=>$dataNoticia,
            ":noticia_Ativo"=>$noticiaAtivo,
            ":noticia"=>$noticia        
        ));
        
    }
    
    public function updateVisita($id){
        $sql = new Sql();
        $sql->query("UPDATE  noticias SET visita_noticia = visita_noticia +1 WHERE id_noticia = id",
        array(":id"=>$id
    ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM noticias WHERE id_noticia = :id",
        array(":id"=>$this->getId()));
    }

    public function __construct($idCategoria="", $tituloNoticia="", $imgNoticia="", $visitaNoticia="", $dataNoticia="", $noticiaAtivo="", $noticia=""){       
        
        $this->idCategoria=$idCategoria;
        $this->tituloNoticia=$tituloNoticia;
        $this->imgNoticia=$imgNoticia;
        $this->visitaNoticia=$visitaNoticia;
        $this->dataNoticia=$dataNoticia;
        $this->noticiaAtivo=$noticiaAtivo;
        $this->noticia=$noticia;
    }

    public function __toString(){
        return jason_encode(array(
            "id_Categoria"=>$this->idCategoria,
            "titulo_Noticia"=>$this->tituloNoticia,
            "img_Noticia"=>$this->imgNoticia,
            "visita_Noticia"=>$this->visitaNoticia,
            "data_Noticia"=>$this->dataNoticia,
            "noticia_Ativo"=>$this->noticiaAtivo,
            "noticia"=>$this->noticia
        ));
    }



}

?>