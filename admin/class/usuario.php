<?php
class Usuario{
private $id;
private $nome;
private $email;
private $senha;


public function getId(){
    return $this->id;
}
public function setId($value){
    $this->id = $value;
}
public function getNome(){
    return $this->nome;
}
public function setNome($value){
    $this->nome = $value;
}
public function getEmail(){
    return $this->email;
}
public function setEmail($value){
    $this->email = $value;
}

public function getSenha(){
    return $this->senha;
}
public function setSenha($value){
    $this->senha = $value;
}

public function loadByid($_id){
    $sql = new Sql();
    $results = $sql->select("SELECT * FROM usuario WHERE id = :id",array(':id'=>$_id));
    if (count($results)>0){
        $this->setData($results[0]);
    }

}

public static function getLista(){
$sql = new Sql();
$results = $sql->select("SELECT * FROM usuario order by email");
}

public static function search ($email_usuario){
    $sql = new Sql();
    return $sql->select("SELECT * FROM usuario WHERE email LIKE :email",array(":email"=>"%".$email_usuario."%"));
}

public function efetuarlogin($_email, $_senha){
    $sql = new Sql();
    $senha_cript = md5($_senha);
    $results = $sql->select("SELECT * FROM usuario WHERE email = :email AND senha = :senha",array(':email'=>$_email,":senha"=>$senha_cript));
    if (count($results)>0){
        $this->setData($results[0]);
    }
}
public function setData($dados){
    $this->setId($dados['id']);
    $this->setNome($dados['nome']);
    $this->setEmail($dados['email']);
    $this->setFoto($dados['foto']);
    $this->setSenha($dados['senha']);
}
public function insert(){
    $sql = new Sql();
        $results = $sql->select("CALL sp_usuario_insert(:nome, :email, :foto, :senha)",
        array(
            ":nome"=>$this->getNome(),
            ":email"=>$this->getEmail(),
            ":foto"=>$this->getFoto(),
            ":senha"=>md5($this->getSenha())
        ));
    if (count($results)>0) {
        $this->setData($results[0]);
    }

}
public function update($_id, $_nome, $_email, $_foto, $_senha){
    $sql = new Sql();
    $sql->query("UPDATE usuario SET nome = :nome, email = :email, foto = :foto, senha = :senha WHERE id = :id",
    array(
        ":nome"=>$_nome,
        ":email"=>$_email,
        ":foto"=>$_foto,
        ":senha"=>$_senha

    ));
}
public function delete(){
    $sql = new Sql();
    $sql->query("DELETE * FROM usuario WHERE id == :id",array(":id"=>$this->getId()));

}
public function __construct($_nome="",$_email="",$_foto="",$_senha="")
    {
        $this->nome = $_nome;
        $this->email = $_email;
        $this->foto = $_foto;
        $this->senha = $_senha;
    }

    
}


?>