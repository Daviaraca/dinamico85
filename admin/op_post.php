<?php
    require_once('../config.php');

    #Inserindo POST    
    if(isset($_POST['btn_cadastrar_post']))
    {
        $post = new Post();
        $post->inserirPost($_POST['id_categoria_post'],$_POST['titulo_post'],$_POST['visita_post'],$_POST['descricao_post'],
        $_POST['img_post'],$_POST['data_post'],isset($_POST['ativo_post'])?'s':'n');
        if($post->getId()>0)
        {
            header('location:principal.php?link=5&msg=inserido');
        }
        else
        {
            header('location:principal.php?link=4&msg=erro');
        }
    }    
    #delete post
    if(isset($_GET['id_post']))
    {        
        $post = new Post();
        $post->deletePost($_GET['id_post']);
        header('location:principal.php?link=&msg=excluido');        
        $_GET['excluir'] = null;
        $_GET['id_post'] = null;
    }
    #Update post
    if(isset($_POST['id']) && isset($_POST['alterar']))
    {
        $post = new Post();
        $post->updatePost($_POST['id_post'],$_POST['id_categoria'],$_POST['titulo'],$_POST['descricao'],$_POST['img'],$_POST['visitas'],$_POST['data'],isset($_POST['ativo'])?'s':'n');
        header('location:principal.php?link=&msg=alterado');
    }
?>