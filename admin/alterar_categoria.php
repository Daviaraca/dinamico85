<?php
$id= filter_input(INPUT_GET,'id_categoria');
$categoria= filter_input(INPUT_GET,'categoria');
$ativ= filter_input(INPUT_GET,'cat_ativ');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Alteração de categoria</title>
</head>
<body>
    <form action="op_categoria.php" method="post" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração de categoria</legend>
            <div>
                <input type="hidden" name="id_categoria" value="<?php echo $id; ?>">
            </div>
            <div>
                <label for="">Categoria</label>
                <input type="text" name="categoria" value="<?php echo $categoria; ?>">
            </div>
            <div>
                    <label for="">Ativo</label>
                    <input type="checkbox" name="check_ativo" <?php echo filter_input(INPUT_GET,'ativ')=='s'?'checked':'';?>>
                </div>
            <div>
                <input type="submit" name="alterar" value="Registrar Alteração">
            </div>
            
        </fieldset>
    </form>
</body>
</html>