<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista Usuarios</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<table id="tb_usuario" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">ID</font></th>
            <th width="60%" height="2"><font size="2" color="#fff">Nome</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Email</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Foto</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php
        require_once('../config.php');
        $users = Usuario::getLista();
        foreach($users as $user){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#0cc"><?php echo $user['id'];?> </font></td>
            <td><font size="2" face="verdana, arial" color="#0cc"><?php echo $user['nome'];?> </font></td>
            <td><font size="2" face="verdana, arial" color="#0cc"><?php echo $user['email'];?> </font></td>
            <td><font size="2" face="verdana, arial" color="#0cc"><?php echo $user['foto'];?> </font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="<?php echo "alterar_usuario.php?id=".$user['id']."&nome=".$user['nome']."&email=".$user['email']."&foto=".$user['foto'];?>">Alterar</a></font></td>
            <td align="center"><font size="2" face="verdana, arial" color="#fff">
                <a href="<?php echo "login.php?excluir=1&id=".$user['id']; ?>">Excluir</a
            </font></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>