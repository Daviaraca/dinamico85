<?php
require_once('../config.php');

if (isset($_POST['cadastro'])){
    $user = new Usuario(
        $_POST['nome'],
        $_POST['email'],
        $_POST['foto'],
        $_POST['senha']
    );
    // inserindo usuario
    $user->insert();
    if($user->getId()!=null){
        header('location:principal.php?link=13&msg=ok');
    }
    else
    {
        header('location:principal.php?link=12&msg=erro');
    }
}

$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id) && $excluir==1){
    $user = new Usuario();
    $user->setId($id);
    $user->delete();
    header('location:principal.php?link=13&msg=ok');
}
else
{
    header('location:principal.php?link=12&msg=erro');
}

if (isset($_POST['alterar'])){
    $user = new Usuario();
    $user->update($_POST['id'],$_POST['nome'],$_POST['email'],$_POST['foto'],$_POST['senha']);
    header('location:principal.php?link=13&msg=ok');    
}

?>