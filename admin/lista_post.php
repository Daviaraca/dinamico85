<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista post</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_post" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">Id</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Id da categoria</font></th>
            <th width="60%" height="2"><font size="2" color="#fff">Titulo do Post</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Descrição</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Imagem</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Visitas</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Data da Publicação</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Ativado</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
        </tr>
        <?php 
            require_once('../config.php');
           $poste = Post::getList();
           foreach($poste as $post){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['id_post']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['id_categoria']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['titulo_post']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['descricao_post']; ?></font></td>

            <td><font size="2" face="verdana, arial" color="#cc0">
                <?php echo $post['img_post']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['visitas']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $post['data_post']; ?></font></td>

            <td><font size="2" face="verdana, arial" color="#c0c">
                <?php echo $post['post_ativo']=='1'?'Sim':'Não'; ?></font></td>


            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
            <td align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo 'op_post.php?excluir=1&id='.$post['id_post']; ?>">
                        Excluir
                    </a>
                </font>
            </td>
        </tr>
<?php } ?>
    </table>
    
</body>
</html>