<?php
require_once('../config.php');
if(isset($_POST['btn_cadastrar'])){
     require_once('conexao.php');
    $banner = $_POST['txt_titulo'];
    $link = $_POST['txt_link'];
    $img = $_POST['img_banner'];
    $alt = $_POST['txt_alt'];
    $ativo = isset($_POST['banner_ativo'])?'1':'0';
    $cmd = $cn->prepare("INSERT INTO banner (titulo_banner, link_banner, img_banner, alt, banner_ativo) 
    VALUES (:titulo, :link, :img, :alt, :ativo)");
    $cmd->execute(array(
        ':titulo'=>$banner,
        ':link'=>$link,
        ':img'=>$img,
        ':alt'=>$alt,
        ':ativo'=>$ativo
    ));
    header('location:principal.php?link=9&msg=ok');
}

$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id) && $excluir==1){
    $bann = new Banner();
    $bann->setId($id);
    $bann->delete($_id);
    
    header('location:principal.php?link=9&msg=ok');
}
else{
    header('location:principal.php?link=9&msg=erro');

}

if (isset($_POST['btn_alterar_banner'])){
    $banner = new Banner();
    $banner->update($_POST['id_categoria'],$_POST['categoria'],$_POST['check_ativo']);
    header('location:principal.php?link=9&msg=ok');    
}

?>