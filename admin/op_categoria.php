<?php
if (isset ($_POST ['btn_cadastrar'])) {
    require_once ('conexao.php');
    $categoria = $_POST ['txt_categoria'];
    $ativo = isset ($_POST ['check_ativo'])? '1': '0';
    $cmd = $cn->prepare("INSERT INTO categoria (categoria, cat_ativo)  VALUES (:cat,:ativ)");
    $cmd->execute (array (
        ':cat' => $categoria,
        ':ativ' => $ativo
    ));
    header ('location: principal.php?link=3&msg=ok');
}
// require_once('../config.php');

// if (isset($_POST['btn_cadastrar'])){
//     $categoria = new Categoria(
//         $_POST['categoria'],
//         $_POST['cat_ativ']
//     );
//     $categoria->insert();
//     if($categoria->getId()!=null){
//         header('location:principal.php?link=3&msg=ok');
//     }
// }


if (isset($_POST['alterar'])){
    $cats = new Categoria();
    $cats->update($_POST['_id'],$_POST['_categoria'],$_POST['ativo']);
    header('location:principal.php?link=3&msg=ok');    
}
else
{
    header('location:principal.php?link=3&msg=erro');    
}

$id = filter_input(INPUT_GET,'id');
$excluir = filter_input(INPUT_GET,'excluir');
if(isset($id) && $excluir==1){
    $categoria = new Categoria();
    $categoria->setId($id);
    $categoria->delete();
    header('location:principal.php?link=3&msg=ok');
}


?>


