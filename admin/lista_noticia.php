<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Lista noticia</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <table id="tb_noticia" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#fcfcfc">
        <tr bgcolor="#993300" align="center">
            <th width="15%" height="2"><font size="2" color="#fff">Id</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Id da categoria</font></th>
            <th width="60%" height="2"><font size="2" color="#fff">Titulo da Noticia</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Imagem</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Visitas</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Data da Publicação</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">Noticia Ativa</font></th>
            <th colspan="2" align="center"><font size="2" color="#fff">Opções</font></th>
            <!-- <th colspan="2"><font size="2" color="#fff">Noticia</font></th> -->
        </tr>
        <?php 
            require_once('../config.php');
           $not = Noticia::getList();
           foreach($not as $noticia){
        ?>
        <tr>
            <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $noticia['id_noticia']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $noticia['id_categoria']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $noticia['titulo_noticia']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $noticia['img_noticia']; ?></font></td>

            <td><font size="2" face="verdana, arial" color="#cc0">
                <?php echo $noticia['txt_visita']; ?></font></td>

                <td><font size="2" face="verdana, arial" color="#0cc">
                <?php echo $noticia['data_noticia']; ?></font></td>

            <td><font size="2" face="verdana, arial" color="#c0c">
                <?php echo $noticia['noticia_ativo']=='1'?'Sim':'Não'; ?></font></td>


            <td align="center"><font size="2" face="verdana, arial" color="#fff"><a href="principal.php?link=">Alterar</a></font></td>
            <td align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo 'op_noticia.php?excluir=1&id='.$noticia['id']; ?>">
                        Excluir
                    </a>
                </font>
            </td>
        </tr>
<?php } ?>
    </table>
    
</body>
</html>